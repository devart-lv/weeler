# frozen_string_literal: true

module Weeler
  module VERSION
    MAJOR = 1
    MINOR = 12
    TINY  = 0
    PRE   = nil
    STRING = [MAJOR, MINOR, TINY, PRE].compact.join('.')
  end
end
